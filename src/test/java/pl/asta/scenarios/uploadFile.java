package pl.asta.scenarios;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import java.io.FileNotFoundException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pl.asta.common.BaseClass;
import pl.asta.pages.csvUpload;
import pl.asta.pages.mainPage;

public class uploadFile extends BaseClass {
	
  
	@BeforeClass
	public void setUp(){
		mainPage page = new mainPage(driver);
		page.choose(5);
	  
		Assert.assertEquals(page.task_number(), 5);
  }
	
  @Test
  public void f() throws InterruptedException, FileNotFoundException{
	  
	  csvUpload up = new csvUpload(driver, wait);
	  up.uploadFile("/home/pwilkosz/SeleniumResources/input.csv");
	  Thread.sleep(2000);
	  up.verifyFile();
  }
}
