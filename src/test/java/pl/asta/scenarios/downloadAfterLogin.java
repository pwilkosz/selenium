package pl.asta.scenarios;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pl.asta.common.BaseClass;
import pl.asta.pages.fileDownload;

import pl.asta.pages.loginPage;
import pl.asta.pages.mainPage;

public class downloadAfterLogin extends BaseClass{
	
 
@BeforeClass
  public void setUp(){
	  mainPage page = new mainPage(driver);
	  page.choose(6);
	  AssertJUnit.assertEquals(page.task_number(), 6);
  }
	
  @Test
  public void f() throws InterruptedException {
	  
	  loginPage startPage = new loginPage(driver, wait);
	  startPage.login("tester", "123-xyz", true);
	  
	  fileDownload downloadPage = new fileDownload(driver, wait);
	  downloadPage.downloadFile();
  }
}
