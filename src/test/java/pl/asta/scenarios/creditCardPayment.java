package pl.asta.scenarios;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import pl.asta.common.BaseClass;
import pl.asta.common.DataSource;
import pl.asta.pages.creditCard;
import pl.asta.pages.mainPage;

public class creditCardPayment extends BaseClass {
	public mainPage page;
	public creditCard card;
	@DataProvider
	public Object[][] customData() throws FileNotFoundException, IOException {
		DataSource.path = "/home/pwilkosz/SeleniumResources/zad8.xlsx";
		return DataSource.dataSet();
	}
	
	
	@BeforeClass
	public void setUp(){
		page = new mainPage(driver);
		page.choose(8); 
		AssertJUnit.assertEquals(page.task_number(), 8);
		card = new creditCard(driver, wait);
		
	}
	
	@Test(dataProvider="customData")
    public void f(
    		String cardType,
    		String Name,
    		String cardNumber,
    		String Cvv,
    		String Month,
    		String Year,
    		String Result) {
		card.resetForm();
		card.setCardType(cardType.trim());
		card.setName(Name.trim());
		card.setCardNumber(cardNumber.trim());
		card.setCardCvv(Cvv.trim());
		card.setMonth(Month.trim());
		card.setYear(Year.trim());
		card.sendForm(Boolean.valueOf(Result));
  }
}
