package pl.asta.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pl.asta.common.BaseClass;

public class loginPage extends BaseClass{
	WebDriver driver;
	WebDriverWait wait;
	By login = By.id("LoginForm__username");
	By password = By.id("LoginForm__password");
	By submit = By.id("LoginForm_save");
	By logout= By.id("logout");
	public loginPage(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
	}
	public void login(String login, String password, boolean success){
		driver.findElement(this.login).sendKeys(login);
		driver.findElement(this.password).sendKeys(password);
		driver.findElement(submit).click();
		Assert.assertEquals(exists(logout), success);	
	}
	
}
