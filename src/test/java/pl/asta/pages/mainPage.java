package pl.asta.pages;
import java.util.regex.*;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.*;

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;		
import org.testng.Assert;

public class mainPage {
	WebDriver driver;
	public mainPage(WebDriver driver){
		this.driver = driver;
	}
	public void choose(int number){
		driver.findElement(By.linkText("Zadanie " + number)).click();
	}
	public int task_number(){
		String label = driver.findElement(By.xpath("//li[contains(text(), 'Zadanie')]")).getText();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(label);
		if (m.find()){
			return Integer.parseInt(m.group());
		}
		else
			return 0;
	}
}
