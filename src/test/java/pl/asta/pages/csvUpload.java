package pl.asta.pages;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import pl.asta.common.BaseClass;
import org.openqa.selenium.support.ui.Select;

public class csvUpload extends BaseClass{
	String filePath;
	WebDriver driver;
	WebDriverWait wait;
	By fileInput = By.xpath("//input[@type='file']");
	By table = By.tagName("tbody/tr");
	public csvUpload(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait= wait;
	}
	public void uploadFile(String path){
		filePath = path;
		driver.findElement(fileInput).sendKeys(filePath);
	}
	public void verifyFile() throws FileNotFoundException{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		List<WebElement> tableRows = driver.findElements(table);
		for (WebElement el : tableRows){
			try{
				List<WebElement> tableCells = el.findElements(By.tagName("td"));
				String[] fileValues = reader.readLine().split(",");
				for (int i = 0; i < fileValues.length; i++){
					Assert.assertEquals(tableCells.get(i).getText(), fileValues[i]);
				}
				
			}
			catch(FileNotFoundException e){
				Assert.assertFalse(true, "File " + filePath + " not found");
			}
			catch(IOException e){
				Assert.assertFalse(true, "Incorrect number of rows in HTML table");
			}
			catch(IndexOutOfBoundsException e){
				Assert.assertFalse(true, "Incorrect number of columns in HTML table");
			}
		}
	}
}
