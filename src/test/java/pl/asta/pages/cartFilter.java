package pl.asta.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import pl.asta.common.BaseClass;
import org.openqa.selenium.support.ui.Select;

public class cartFilter extends BaseClass {
	WebDriver driver;
	WebDriverWait wait;	
	Select categoryList;
	By listArrow = By.className("select2-selection__arrow");
	By inputSearch = By.className("select2-search__field");
	By searchResults = By.xpath("//span[@class='select2-results']/ul/li");
	public cartFilter(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
		categoryList = new Select(driver.findElement(By.cssSelector(".js-category-select")));
	}
	public void checkIfCategoryFiltered(String categoryName){
		List<WebElement> itemList = driver.findElements(By.xpath("//div[@class='product-list']//div[@class='thumbnail']/strong"));
		for(WebElement we : itemList){
			Assert.assertEquals(we.getText(), categoryName);
		}	
	}
	public void searchFor(String query, String name){
		driver.findElement(listArrow).click();
		driver.findElement(inputSearch).sendKeys(query);
		List<WebElement> results = driver.findElements(searchResults);
		for (WebElement el : results){
			if(!el.getText().contains(query)){
				Assert.assertFalse(false, String.format("searching for: %s gave result: %s", query, el.getText()));
			}
			softAssert.assertTrue(el.getText().contains(query), "given search query dose not match");
			if (el.getText().equalsIgnoreCase(name)){
				el.click();
				checkIfCategoryFiltered(name);
			}
		}
	}
	public void select(String name){
		driver.findElement(listArrow).click();
		driver.findElement(By.xpath("//span[@class='select2-results']/ul/li[text() = '" + name + "']")).click();
		checkIfCategoryFiltered(name);
	}
}
