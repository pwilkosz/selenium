package pl.asta.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import pl.asta.common.BaseClass;
import org.openqa.selenium.support.ui.Select;

public class jobOffer extends BaseClass{
	WebDriver driver;
	WebDriverWait wait;
	String baseWindowHandle;
	String name, email, phone;
	Pattern emailPattern = Pattern.compile("[a-zA-Z]+@[a-zA-Z]+\\.[a-zA-Z]+");
	Pattern phonePattern = Pattern.compile("\\d{3}-\\d{3}-\\d{3}");
	By formName = By.xpath("//input[@name='name']");
	By formEmail = By.xpath("//input[@name='email']");
	By formPhone = By.xpath("//input[@name='phone']");
	By formSubmit = By.id("save-btn");
	public jobOffer(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
	}
	
	public void openContactForm(){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='APLIKUJ']")));
		baseWindowHandle = driver.getWindowHandle();
		driver.findElement(By.xpath("//button[text()='APLIKUJ']")).click();
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		Assert.assertTrue(driver.getWindowHandles().size() == 2);
		List<String> handlesList = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(handlesList.get(1));
		WebElement iframe = driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(iframe);
		wait.until(ExpectedConditions.visibilityOfElementLocated(formName));
	}
	public void setName(String name){
		driver.findElement(formName).sendKeys(name);
		this.name = name;
	}
	
	public void setEmail(String email){
		driver.findElement(formEmail).sendKeys(email);
		this.email = email;
	}
	
	public void setPhone(String phone){
		driver.findElement(formPhone).sendKeys(phone);
		this.phone = phone;
	}
	
	public void submitForm(){
		//Submit the form
		driver.findElement(formSubmit).click();
		//check if provided values are correctly formatted
		
		//e-mail: pattern is a@b.pl
		Matcher emailMatcher = emailPattern.matcher(email);
		if(!emailMatcher.matches()){
			try{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Nieprawidłowy email']")));
				Assert.assertTrue(true, "warning visible");
			}
			catch(NoSuchElementException e){
				softAssert.assertFalse(true, "Object not found");
			}
		}
		//phone
		Matcher phoneMatcher = phonePattern.matcher(phone);
		if(!phoneMatcher.matches()){
			try{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Zły format telefonu')]")));
				Assert.assertTrue(true, "warning visible");
			}
			catch(NoSuchElementException e){
				softAssert.assertFalse(true, "Object not found");
			}
		}
		if(emailMatcher.matches() && phoneMatcher.matches()){
			try{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text() = 'Wiadomość została wysłana']")));
				Assert.assertTrue(true, "Form successfully sent");
			}
			catch(NoSuchElementException e){
				Assert.assertFalse(true, "Error during form processing");
			}
		}
		
	}
}
