package pl.asta.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pl.asta.common.BaseClass;

public class creditCard extends BaseClass {
	WebDriver driver;
	WebDriverWait wait;	
	Select cardType;
	Select month;
	Select year;
	// LOCATORS
	By cardTypeSelect = By.id("task8_form_cardType");
	By name = By.id("task8_form_name");
	By cardNumber = By.id("task8_form_cardNumber");
	By cardCvv = By.id("task8_form_cardCvv");
	By monthSelect = By.id("task8_form_cardInfo_month");
	By yearSelect = By.id("task8_form_cardInfo_year");
	By form = By.id("task8_form");
	
	By reset = By.id("main-reset");
	
	By success = By.className("alert-success");
	//--------------------------------------------------
	public creditCard(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
		cardType = new Select(this.driver.findElement(cardTypeSelect));
		month = new Select(this.driver.findElement(monthSelect));
		year = new Select(this.driver.findElement(yearSelect));
	}
	
	public void resetForm(){
		driver.findElement(reset).click();
		cardType = new Select(this.driver.findElement(cardTypeSelect));
		month = new Select(this.driver.findElement(monthSelect));
		year = new Select(this.driver.findElement(yearSelect));
	}
	
	public void setCardType(String type){
		
		cardType.selectByVisibleText(type);
	}
	
	public void setName(String Name){
		driver.findElement(this.name).sendKeys(Name);
	}
	
	public void setCardNumber(String number){
		driver.findElement(cardNumber).sendKeys(number);
	}
	
	public void setCardCvv(String cvv){
		driver.findElement(cardCvv).sendKeys(cvv);
	}
	
	public void setMonth(String month){
		this.month.selectByVisibleText(month);
	}
	
	public void setYear(String year){
		this.year.selectByVisibleText(year);
	}
	
	public void sendForm(boolean value){
		System.out.println("boolean value is " + value);
		driver.findElement(form).submit();
		Assert.assertEquals(exists(success), value);
	}
	
	
}
