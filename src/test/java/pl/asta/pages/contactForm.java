package pl.asta.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import pl.asta.common.BaseClass;
import org.openqa.selenium.support.ui.Select;


public class contactForm extends BaseClass{
	WebDriver driver;
	WebDriverWait wait;	
	By name = By.id("in-name");
	By surname = By.id("in-surname");
	By notes = By.id("in-notes");
	By phone = By.id("in-phone");
	By menu = By.xpath("//a[@class='dropdown-toggle menu-border']");
	By form = By.xpath("//ul[@id='menu1']//a[text()='Formularz']");
	By enable = By.linkText("Przejdź do trybu edycji");
	public contactForm(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
	}
	public boolean isEnabled(){
		return driver.findElement(name).isEnabled();
	}
	public void Enable() throws InterruptedException{
		WebElement menuLink = driver.findElement(menu);
		Actions builder = new Actions(driver);
		Thread.sleep(3000);
		builder.moveToElement(menuLink).build().perform();;
		Thread.sleep(3000);
		
		Thread.sleep(3000);
		//driver.findElement(menu).click();
		//driver.findElement(form).click();
		//driver.findElement(enable).click();
		
	}
}
