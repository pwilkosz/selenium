package pl.asta.pages;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;;

public class productCart {
	WebDriver driver;
	WebDriverWait wait;
	float totalPrice;
	int totalQuantity;
	
	public productCart(WebDriver driver, WebDriverWait wait) throws InterruptedException{
		this.driver = driver;
		this.wait = wait;
		this.totalPrice = 0;
		this.totalQuantity = 0;
	}
	public void clearCart() throws InterruptedException{
		List<WebElement> cartList = driver.findElements(By.xpath("//button[text()='Usuń']"));
		for(WebElement w : cartList){
			w.click();
		}
		
	}
	
	public void deleteFromCart(String name){
		WebElement deleteBtn = driver.findElement(By.xpath("//div[@class='panel-body']//div[contains(text(), '" + name + "')]//ancestor::div[@class='row-in-basket']//button"));
		deleteBtn.click();
		Assert.assertEquals(checkCartFor(name), 0);
	}
	
	public float calculateItemsPrice(){
		try{
			Float totalPrice = 0f;
			String txtInit, itemName;
			Pattern itemNamePattern = Pattern.compile("[a-zA-Z]+");
			List<WebElement> cartItems = driver.findElements(By.xpath("//div[@class='row row-in-basket']"));
			for (WebElement el: cartItems){
				txtInit = el.findElement(By.xpath("div[1]")).getText();
				Matcher m = itemNamePattern.matcher(txtInit);
				m.find();
				itemName = m.group();
				//find price and quantity
				totalPrice += checkCartFor(itemName) * checkCartPrice(itemName);
				totalPrice = Math.round(totalPrice*100)/100.0f;
			}
			return totalPrice;
		}
		catch(NoSuchElementException e){
			return 0;
		}
		
	}
	
	public float cartTotalPrice(){
		Pattern totalPrice = Pattern.compile("[0-9]+\\.[0-9]+");
		String summary = driver.findElement(By.xpath("//span[@class='summary-price']")).getText();
		Matcher m = totalPrice.matcher(summary);
		m.find();
		return Float.parseFloat(m.group());
	}
	
	public int checkCartFor(String name){
		try{
			String q = driver.findElement(By.xpath("//span[@data-quantity-for='" + name + "']")).getText();
			return Integer.parseInt(q);
		}
		catch(NoSuchElementException e){
			return 0;
		}
		
	}
	
	public float checkItemPrice(String name){
		try{
			Pattern productPricePattern = Pattern.compile("[0-9]+\\.[0-9]+");
			String productPrice = driver.findElement(By.xpath("//form//h4[text() = '" + name + "']//ancestor::div[@class='thumbnail']/div[@class='caption']/p[1]")).getText();
			Matcher m = productPricePattern.matcher(productPrice);
			m.find();
			return Float.parseFloat(m.group());
		}
		catch(NoSuchElementException e){
			return 0;
		}
		
	}
	
	public float checkCartPrice(String name){
		try{
			Pattern cartPricePattern = Pattern.compile(".*\\((.*)\\).*");
			String cartPrice = driver.findElement(By.xpath("//div[@class='panel-body']//div[contains(text(), '" + name + "')]")).getText();
			Matcher m = cartPricePattern.matcher(cartPrice);
			m.find();
			return Float.parseFloat(m.group(1));
		}
		catch(NoSuchElementException e){
			return 0;
		}
		
	}
	
	public void addToCart(String name, int quantity) throws InterruptedException{
		int newQuantity = quantity + this.checkCartFor(name);
		WebElement product = driver.findElement(By.xpath("//form//h4[text() = '" + name + "']//ancestor::div[@class='thumbnail']"));
		product.findElement(By.xpath("div/div/input")).clear();
		product.findElement(By.xpath("div/div/input")).sendKeys(Integer.toString(quantity));
		wait.until(ExpectedConditions.elementToBeClickable(product.findElement(By.xpath("div/div/span/button"))));
		product.findElement(By.xpath("div/div/span/button")).click();
		Assert.assertEquals(this.checkCartFor(name), newQuantity);
		Assert.assertEquals(checkItemPrice(name), checkCartPrice(name));
		
	}
	
}
