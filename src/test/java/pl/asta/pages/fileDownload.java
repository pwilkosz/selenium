package pl.asta.pages;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pl.asta.common.BaseClass;

public class fileDownload extends BaseClass{
	String FILE_NAME = "pgs_cv.jpg";
	WebDriver driver;
	WebDriverWait wait;
	By download = By.linkText("Pobierz plik");
	public fileDownload(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
	}
	public void downloadFile() throws InterruptedException{
		driver.findElement(download).click();
		Thread.sleep(5000);
		File file = new File(DOWNLOAD_DIRECTORY + FILE_NAME);
		System.out.println(DOWNLOAD_DIRECTORY + FILE_NAME);
		Assert.assertTrue(file.exists());
	}
}
