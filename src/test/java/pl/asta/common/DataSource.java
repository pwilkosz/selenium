package pl.asta.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;

import org.apache.poi.xssf.usermodel.XSSFRow;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;

public final class DataSource {
	public static String path;
	private static XSSFSheet sheet;
	private static XSSFWorkbook workBook;
	private static XSSFCell cell;
	private static XSSFRow row;
	
	private DataSource() {
	
	}
	
	@SuppressWarnings("deprecation")
	public static String getCellData(int row, int col){
		try {
			cell = sheet.getRow(row).getCell(col);
			cell.setCellType(CellType.STRING);
			return cell.getStringCellValue();
			/*switch(cell.getCellTypeEnum()){
				case BLANK: return "";
				break;
				case NUMERIC: return cell.getNumericCellValue();
				break;
				case STRING: return cell.getStringCellValue();
				break;
				case BOOLEAN: return cell.getBooleanCellValue();
				break;
				default:
					throw(new Exception("Unknown type cannot be handled!"));}*/
					
			
			
		}
		catch (Exception e){
			Assert.assertFalse(true, e.toString());
			throw(e);
		}
	}
	
	public static Object[][] dataSet() throws FileNotFoundException, IOException{
		try{
			workBook = new XSSFWorkbook(new FileInputStream(path));
			sheet = workBook.getSheet("Sheet1");
			int Rows = sheet.getLastRowNum();
			int Cols = 7;
			String[][] outputTable = new String[Rows][Cols];
			for (int i = 0; i < Rows; i++)
				for (int j = 0; j < Cols; j++){
					System.out.println(i);
					System.out.println(j);
					outputTable[i][j] = getCellData(i, j);
				}
			return outputTable;
		}
		catch(FileNotFoundException e){
			Assert.assertFalse(true, "Specified file cannot be found");
			throw e;
		}
		catch(Exception e){
			Assert.assertFalse(true, e.toString());
			
			throw e;
		}
	}
}
