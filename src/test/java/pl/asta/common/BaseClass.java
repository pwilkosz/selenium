package pl.asta.common;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;		
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.apache.commons.io.FileUtils;
	
	

public class BaseClass {			
		protected String DOWNLOAD_DIRECTORY = "/home/pwilkosz/SeleniumResources/Downloads/";
		String url = "https://testingcup.pgs-soft.com/";
		protected static WebDriver driver;	
		protected static WebDriverWait wait;
		protected static SoftAssert softAssert;
		
		@BeforeClass				
		public void setup() throws IOException {	
			FileUtils.cleanDirectory(new File(DOWNLOAD_DIRECTORY));
			System.setProperty("webdriver.gecko.driver", "/home/pwilkosz/workspace/drivers/geckodriver");
		    FirefoxProfile fp = new FirefoxProfile();
		    fp.setPreference("browser.download.folderList",2);
		    fp.setPreference("browser.download.manager.showWhenStarting",false);
		    fp.setPreference("browser.download.dir", DOWNLOAD_DIRECTORY);
		    //fp.setPreference("browser.helperApps.alwaysAsk.force", false);
		    fp.setPreference("browser.helperApps.neverAsk.saveToDisk","image/jpeg, text/html ,application/xml");
			driver = new FirefoxDriver(fp); 
		    wait = new WebDriverWait(driver, 30);
		    softAssert = new SoftAssert();
		    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.get(this.url);  
			String title = driver.getTitle();
			AssertJUnit.assertEquals(title, "Welcome!");
			
		}	
		
		
		@AfterClass
		public void tearDown(){
			driver.quit();
		}
		
		public boolean exists(By elem){
			try{
				driver.findElement(elem);
				return true;
			}
			catch(NoSuchElementException e){
				return false;
			}
		}
					
		
}